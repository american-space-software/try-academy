import { Container, initTestContainer } from "space-mvc";

import { StudentService } from "../src/service/student-service";
import { StudentDao } from "../src/dao/student-dao";

import { mainStoreDefinitions } from "../src/store-definitions"
import { CourseService } from "../src/service/course-service";
import { CourseDao } from "../src/dao/course-dao";

let container:Container 

export async function getTestContainer() {

    if (container) return container

    container = new Container()

    container.bind(StudentService).toSelf().inSingletonScope()
    container.bind(StudentDao).toSelf().inSingletonScope()

    container.bind(CourseService).toSelf().inSingletonScope()
    container.bind(CourseDao).toSelf().inSingletonScope()

    return initTestContainer(container, mainStoreDefinitions)

}
