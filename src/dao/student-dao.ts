import { dao, MetastoreService, Initializable } from "space-mvc";
import { Student } from "../dto/student";

@dao()
class StudentDao implements Initializable {

    private store

    constructor(
        private metastoreService:MetastoreService
    ) {}

    async init() {
        this.store = this.metastoreService.getStore("student") 
    }

    async put(key:string, student:Student) {
        return this.store.put(key, student)
    }

    async get(key:string): Promise<Student> {
        return this.store.get(key)
    }

    async list(offset?: number, limit?: number) : Promise<Student[]> {
        return this.store.list(offset, limit)
    }

}

export {
    StudentDao
}