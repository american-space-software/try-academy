import "core-js/stable"
import "regenerator-runtime/runtime"

import SpaceMVC, {IpfsService } from "space-mvc";

import { container } from "./inversify.config"
import { mainStoreDefinitions } from "./store-definitions"

//Get main app component.
import AppComponent from './components/app.f7.html'

//Import CSS
import './html/css/framework7.bundle.min.css'
import './html/css/framework7-icons.css'
import './html/css/app.css'



export default async () => {

    SpaceMVC.getEventEmitter().on('spacemvc:initFinish', async () => {

        SpaceMVC.app().panel.create({
            el: '.panel-left',
            visibleBreakpoint: 1024
        })

        await _listenForPeers()

    })

    let f7Params = {
        root: '#app', // App root element
        id: 'tryschool', // App bundle ID
        name: 'TRY School', // App name
        theme: 'auto', // Automatic theme detection
        component: AppComponent
    }


    await SpaceMVC.init(container, f7Params, "0x88bfEE1f69e83aa5F146B2B6F527878866D39fE9", mainStoreDefinitions)

}




async function _listenForPeers() {

    _updateConnectedPeers(0)

    let ipfsService = await SpaceMVC.getContainer().get<IpfsService>(IpfsService)

    ipfsService.ipfs.libp2p.connectionManager.on('peer:connect', async function(peer) {            
        let peers = await ipfsService.ipfs.swarm.peers()
        _updateConnectedPeers(peers.length)
    })


    ipfsService.ipfs.libp2p.connectionManager.on('peer:disconnect', async function(peer) {            
        let peers = await ipfsService.ipfs.swarm.peers()
        _updateConnectedPeers(peers.length)
    })
}


function _updateConnectedPeers(peers:number) {
    SpaceMVC.app().rootComponent.$setState({ connectedPeers: peers })
}